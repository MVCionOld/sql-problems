SELECT
    min(height) AS min_height,
    max(height) AS max_height,
    min(weight) AS min_weight,
    max(weight) AS max_weight
FROM
    public.hw
